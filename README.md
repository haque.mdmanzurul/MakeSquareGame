# MakeSquareGame
This is small puzzle game I wrote when I was learning Java during 2007. Its based on J2SE and wrote this game in J2SE version 1.6 (dont remember exactly though) and later updated it to JDK 1.8
In this small game you can try your puzzle solve brain. This game has 4 levels and in each level you get different challenge. The game is about making square boxes. 

Once you run the game, you will see there are square boxes made up using fire stick and if you click on the fire stick they will dis-appear and make new squares. 

Challenge is to remove a certain number of fire sticks and make certain number of squares. 
P.S. There are multiple ways to solves each step


# How to run
From terminal just run ran.bat file and please remember to check that in bat file you have the correct path of your JDK installation.
